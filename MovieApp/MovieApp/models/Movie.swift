//
//  Movie.swift
//  MovieApp
//
//  Created by admin on 2018-10-26.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation

class Movie : NSObject
{
    var image: String?
    var title: String?
    var desc: String?
    
    init (image: String?, title:String?, desc: String?)
    {
        super.init()
        self.image = image
        self.title = title
        self.desc=desc
    }
}
