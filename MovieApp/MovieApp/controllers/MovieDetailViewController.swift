//
//  MovieDetailViewController.swift
//  MovieApp
//
//  Created by Davoodi, Alireza on 2018-10-30.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class MovieDetailViewController: UIViewController {

    var movie: Movie?  = nil

    @IBOutlet weak var movieImageView: UIImageView!

    @IBOutlet weak var titleTextField: UITextField!

    @IBOutlet weak var descriptionTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {

        if let img = self.movie?.image
        {
            self.movieImageView.image = UIImage(named: img)
        }

        if let mtitle = self.movie?.title
        {
            self.titleTextField.text = mtitle
        }

        if let mDesc = self.movie?.desc
        {
            self.descriptionTextField.text = mDesc
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
