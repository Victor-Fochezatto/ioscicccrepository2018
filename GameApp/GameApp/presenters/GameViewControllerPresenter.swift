//
//  GameViewControllerPresenter.swift
//  GameApp
//
//  Created by admin on 2018-10-19.
//  Copyright © 2018 Davoodi, Alireza. All rights reserved.
//

import Foundation

protocol GameViewControllerPresenterProtocl : AnyObject{
    
    func gameViewModelDidChanged (presenter: GameViewControllerPresenter, viewModel: GameViewModel)
}

class GameViewControllerPresenter : ViewControllerPresenter
{
    
    let rpsGameUpdateNotification = NotificationCenter.default
    
    var gameManager: GameManager!
    
    weak var delegate: GameViewControllerPresenterProtocl?
    
    init(viewController: GameViewController) {
        super.init()
        self.delegate = viewController
        rpsGameUpdateNotification.addObserver(self, selector: #selector(self.rpsGameModelDidUpdated), name: NSNotification.Name(rawValue: Constants.NotificationConstants.RPS_GAME_STATUS_UPDATE_NOTIFICATION), object: nil)
        
        self.gameManager = GameManager.getGameManagerSharedInstance()
    }
    
    @objc func rpsGameModelDidUpdated()->Void
    {
        let rpsG = GameManager.getGameManagerSharedInstance().rpsGame
        if let rpsGame = rpsG
        {
            let gameViewModel: GameViewModel!
            gameViewModel = GameViewModel(player1Score: rpsGame.player1.numberOfWin, player2Score: rpsGame.player2.numberOfWin, player1FirstName: rpsGame.player1.name, player2FirstName: rpsGame.player2.name, player1Image: rpsGame.player1.imgURL, player2Image: rpsGame.player2.imgURL)
            
            if let del = self.delegate
            {
                del.gameViewModelDidChanged(presenter: self, viewModel: gameViewModel)
            }
            //gameViewModel.player1SymbolsStatus = rpsGame.
        }
    }
}
